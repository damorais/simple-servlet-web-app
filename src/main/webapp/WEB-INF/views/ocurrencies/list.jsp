<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="ifsp" %>

<!DOCTYPE html>
<html>
	<head>
		<meta charset='utf-8'>
		<meta name='viewport'
			content='width=device-width, initial-scale=1, shrink-to-fit=no'>
		
		<ifsp:bootstrapCSS />
		
	</head>
	<body>
		<div id='content' class='container-fluid'>
			<%-- <c:if test="${not empty flashMessage}">
				<div class="alert alert-success alert-dismissible fade show" role="alert">
				  ${ flashMessage }
				  <c:remove var="flashMessage" scope="session" /> 
				</div>
			</c:if> --%>
			<div class="row">
				<div class="col-md-9">Olá, ${ UserAuthenticated }</div>
				<div class="col-md-3"><a href="logout">Logout</a></div>
			</div>
			
			<h1>Cliente: ${ customer.name }</h1>
			
			<div>
			<h2>Nova Ocorrência</h2>
				<form method='POST' action="/clientes/ocorrencias?customerId=${customer.customerId}">
					<div class="form-group">
						<textarea class="form-control" placeholder="Entre com a ocorrência"
							id="txtDescription" name="txtDescription"></textarea>
					</div>
				
					<button type="submit" class="btn btn-primary">Salvar</button>
				</form>
			</div>
			<div class="row">
				<h2>Ocorrências</h2>
				<ul class="list-group">
				<c:forEach var="ocurrency" items="${ ocurrencies }">
					<li class="list-group-item">
						<fmt:formatDate value="${ocurrency.createdAt.time}" 
							var="formattedDate"  type="date" pattern="dd-MM-yyyy hh:mm:ss" />
						<b>${formattedDate}: </b> ${ocurrency.description}
					</li>
				</c:forEach>
				</ul>			
			</div>
			<div class="row">
				<a href="/clientes" class="btn btn-primary" role="button" aria-pressed="true">Voltar</a>
			</div>
		</div>
		
		<ifsp:bootstrapJS />
		
	</body>
</html>