<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="ifsp" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset='utf-8'>
		<meta name='viewport'
			content='width=device-width, initial-scale=1, shrink-to-fit=no'>
		<ifsp:bootstrapCSS></ifsp:bootstrapCSS>
	</head>
	<body>
		<div id='content' class='container-fluid'>
			<div>
				<form method='POST' action="/clientes?action=edit&customerId=${ customer.customerId }">
					<input type="hidden" id="hdnCustomerId" name="hdnCustomerId" value="${ customer.customerId }">  
					<div class="form-group">
						<label for="txtName">Nome</label>
						<input type="text" class="form-control" placeholder="Entre com o nome do cliente"
						 	id="txtName" name="txtName" value="${ customer.name }">
					</div>
					<div class="form-group">
						<label for="txtEmail">Email</label>
						<input type="email" class="form-control" placeholder="Entre com o email do cliente"
							id="txtEmail" name="txtEmail" value="${ customer.email }">
					</div>
					<div class="form-group">
						<label for="txtStreet">Endereço</label>
						<input type="text" class="form-control" placeholder="Entre com o endereço"
							id="txtStreet" name="txtStreet" value="${ customer.getAddress().getStreet() }">
					</div>
					<div class="form-group">
						<label for="txtComplement">Complemento</label>
						<input type="text" class="form-control" placeholder="Entre com o complemento"
							id="txtComplement" name="txtComplement" value="${ customer.getAddress().getComplement() }">
					</div>	
					<div class="form-group">
						<label for="txtCity">Cidade</label>
						<input type="text" class="form-control" placeholder="Entre com a cidade"
							id="txtCity" name="txtCity" value="${ customer.getAddress().getCity() }">
					</div>
					
					<div class="form-group">
						<label for="txtState">Estado</label>
						<input type="text" class="form-control" placeholder="Entre com o estado"
							id="txtState" name="txtState" value="${ customer.getAddress().getState() }">
					</div>
					<a href="/clientes" class="btn btn-danger active" role="button" aria-pressed="true">Cancelar</a>
					<button type="submit" class="btn btn-primary">Salvar</button>
				</form>
			</div>
		</div>
		<ifsp:bootstrapJS></ifsp:bootstrapJS>
	</body>
</html>