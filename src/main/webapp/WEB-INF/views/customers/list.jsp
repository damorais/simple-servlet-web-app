<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="ifsp" %>

<!DOCTYPE html>
<html>
	<head>
		<meta charset='utf-8'>
		<meta name='viewport'
			content='width=device-width, initial-scale=1, shrink-to-fit=no'>
		
		<ifsp:bootstrapCSS />
		
	</head>
	<body>
		<div id='content' class='container-fluid'>
			<c:if test="${not empty flashMessage}">
				<div class="alert alert-success alert-dismissible fade show" role="alert">
				  ${ flashMessage }
				  <% request.getSession().removeAttribute("flashMessage"); %>
				  <c:remove var="flashMessage" scope="session" /> 
				</div>
			</c:if>
			<div class="row">
				<div class="col-md-9">Olá, ${ UserAuthenticated }</div>
				<div class="col-md-3"><a href="logout">Logout</a></div>
			</div>
			<div class="row">
				<table class="table">
					<thead>
						<tr>
							<th>#</th>
							<th>Nome</th>
							<th>Email</th>
							<th>Endereço</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
					<c:forEach var="customer" items="${ customers }">
						<tr>
							<th scope="row">${ customer.customerId }</th>
							<td>${ customer.name }</td>
							<td>${ customer.email }</td>
							<td>${ customer.address }</td>
							<td>
								<a href="clientes/ocorrencias?customerId=${customer.customerId}">Ver Ocorrências</a> |
								<a href="clientes?action=edit&customerId=${customer.customerId}">Editar</a> | 
								<a href="clientes?action=delete&customerId=${customer.customerId}">Excluir</a>
							</td>
						</tr>
					</c:forEach>
					</tbody>
				</table>
			</div>
			<div class="row">
				<a href="/clientes?action=new" class="btn btn-primary active" role="button" aria-pressed="true">Adicionar</a>
			</div>
		
		</div>
		
		<ifsp:bootstrapJS />
		
	</body>
</html>