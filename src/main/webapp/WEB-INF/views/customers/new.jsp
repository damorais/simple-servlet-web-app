<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="ifsp" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset='utf-8'>
		<meta name='viewport'
			content='width=device-width, initial-scale=1, shrink-to-fit=no'>
		<ifsp:bootstrapCSS></ifsp:bootstrapCSS>
	</head>
	<body>
		<div id='content' class='container-fluid'>
			<c:if test="${not empty errorMessages}">
			<div>
				<c:forEach var="error" items="${ errorMessages }">
					<p>${ error.errorMessage }</p>
				</c:forEach>
			</div>
			</c:if>
			
			<div>
				<form method='POST' action="/clientes?action=new">
					
					<ifsp:inputtext id="txtName" fieldType="text" fieldName="Nome" contentPlaceholder="Entre com o nome do cliente"></ifsp:inputtext>
					<ifsp:inputtext id="txtEmail" fieldType="email" fieldName="Email" contentPlaceholder="Entre com o email do cliente"></ifsp:inputtext>
					
					<ifsp:inputtext id="txtStreet" fieldType="text" fieldName="Endereço" contentPlaceholder="Entre com o endereço"></ifsp:inputtext>
					<ifsp:inputtext id="txtComplement" fieldType="text" fieldName="Complemento" contentPlaceholder="Entre com o complemento"></ifsp:inputtext>
					<ifsp:inputtext id="txtCity" fieldType="text" fieldName="Cidade" contentPlaceholder="Entre com a cidade"></ifsp:inputtext>
					<ifsp:inputtext id="txtState" fieldType="text" fieldName="Estado" contentPlaceholder="Entre com o estado"></ifsp:inputtext>
					
					<a href="/clientes" class="btn btn-danger active" role="button" aria-pressed="true">Cancelar</a>
					<button type="submit" class="btn btn-primary">Salvar</button>
				</form>
			</div>
		</div>
		<ifsp:bootstrapJS></ifsp:bootstrapJS>
	</body>
</html>