<%@ attribute name="id" required="true" %>
<%@ attribute name="fieldType" required="true" %>
<%@ attribute name="fieldName" required="true" %>
<%@ attribute name="contentPlaceholder" %>

<div class="form-group">
	${ fieldType.toUpperCase() }

	<label for="${id}">${fieldName}</label>
	<input type="${fieldType}" class="form-control" id="${id}" name="${id}" placeholder="${contentPlaceholder}">
</div>