package br.edu.ifsp.simpleServlet.filters;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//@WebFilter("/clientes")
public class AuthenticationFilter implements Filter {

	
	@Override
	public void init(FilterConfig fConfig) throws ServletException {

	}
	
	@Override
	public void destroy() {
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		// TODO Auto-generated method stub
		// place your code here

		System.out.println("Passei pelo filtro de autenticação!!!!");
		
		HttpServletRequest httpRequest = (HttpServletRequest)request;
		HttpServletResponse httpResponse = (HttpServletResponse)response;
		
		String userAuthenticated = (String) httpRequest.getSession().getAttribute("UserAuthenticated");
		
		if(userAuthenticated != null) {
			// pass the request along the filter chain
			chain.doFilter(request, response);
		}else {
			httpResponse.sendRedirect("/login");
		}
	}

	

}
