package br.edu.ifsp.simpleServlet.factories;

public abstract class TemplateFactory {
    //Retorna uma string contendo o esqueleto da página + o conteúdo, passado
    //como parâmetro
    public static String GetTemplate(String content){
        StringBuilder sb = new StringBuilder();
        sb.append("<html>");
        sb.append("<head>");
        sb.append("<meta charset='utf-8'>");
        sb.append("<meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>");
        sb.append("<link rel='stylesheet' href='/public/css/bootstrap.min.css'>");
        sb.append("</head>");
        sb.append("<body>");
        sb.append("<div id='content' class='container-fluid'>");

        sb.append(content);

        sb.append("</div>");
        sb.append("<script src='/public/js/jquery-3.2.1.slim.min.js' ></script>");
        sb.append("<script src='/public/js/popper.min.js'></script>");
        sb.append("<script src='/public/js/bootstrap.min.js'></script>");

        sb.append("</body>");
        sb.append("</html>");

        return sb.toString();
    }
}
