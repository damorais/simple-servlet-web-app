package br.edu.ifsp.simpleServlet.factories;

import br.edu.ifsp.simpleServlet.repositories.CustomersRepository;
import br.edu.ifsp.simpleServlet.repositories.JpaCustomersRepository;
import br.edu.ifsp.simpleServlet.repositories.MariaDBCustomersRepository;
import br.edu.ifsp.simpleServlet.repositories.StaticCustomersRepository;

public class CustomersRepositoryFactory {
	public static CustomersRepository GetCustomerRepository(String repositoryType) {		
		CustomersRepository repository = null;
		
		switch(repositoryType) {
		case "MariaDB":
			repository = new MariaDBCustomersRepository();
			break;
		case "JPA":
			repository = new JpaCustomersRepository();
			break;
		case "Static":
		default:
			repository = new StaticCustomersRepository();
			break;
		}
		
		return repository;
	}

}
