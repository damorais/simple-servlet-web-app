package br.edu.ifsp.simpleServlet.repositories;

import br.edu.ifsp.simpleServlet.entities.Customer;
import br.edu.ifsp.simpleServlet.entities.Ocurrence;
import br.edu.ifsp.simpleServlet.entities.Address;

//import java.sql.Connection;
//import java.sql.DriverManager;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class MariaDBCustomersRepository implements CustomersRepository {

//	private static String DBURL = "jdbc:mariadb://localhost/ifsp_crm";
//	private static String USERNAME = "root";
//	private static String PASSWORD = "";
//
//	private static String INSERT_STATEMENT = "INSERT INTO CUSTOMERS(Name, Email, Address) VALUES(?, ?, ?)";
//	private static String UPDATE_STATEMENT = "UPDATE CUSTOMERS SET Name = ?, Email = ?, Address = ? WHERE CustomerID = ?";
//	private static String SELECT_ALL_STATEMENT = "SELECT CustomerId, Name, Email, Address FROM CUSTOMERS";
//	private static String SELECT_ONE_STATEMENT = "SELECT CustomerId, Name, Email, Address FROM CUSTOMERS WHERE CustomerId = ? LIMIT 1";
//	private static String DELETE_ONE_STATEMENT = "DELETE FROM CUSTOMERS WHERE CustomerID = ?";

	@Override
	public List<Customer> getCustomers() {

		List<Customer> customers = new ArrayList<>();
//
//		try (Connection conn = (Connection) DriverManager.getConnection(DBURL, USERNAME, PASSWORD);
//				PreparedStatement stmt = conn.prepareStatement(SELECT_ALL_STATEMENT);
//				ResultSet rs = stmt.executeQuery();) {
//
//			while (rs.next()) {
//
//				long customerId = rs.getLong("CustomerId");
//				String name = rs.getString("Name");
//				String email = rs.getString("Email");
//				String address = rs.getString("Address");
//
//				customers.add(new Customer(customerId, name, email, address));
//
//			}
//
//		} catch (SQLException e) {
//			//Tratamento de exceção incorreto! Estou escondendo o erro...
//			System.out.println(e.getMessage());
//		}

		return customers;
	}

	@Override
	public void save(String name, String email, Address address) {

//		try (Connection conn = (Connection) DriverManager.getConnection(DBURL, USERNAME, PASSWORD);
//				PreparedStatement stmt = conn.prepareStatement(INSERT_STATEMENT);) {
//
//			stmt.setString(1, name);
//			stmt.setString(2, email);
//			stmt.setString(3, address);
//
//			stmt.executeUpdate();
//
//		} catch (SQLException e) {
//			//Tratamento de exceção incorreto! Estou escondendo o erro...
//			System.out.println(e.getMessage());
//		}
	}

	@Override
	public void save(long customerId, String name, String email, Address address) {

//		try (Connection conn = (Connection) DriverManager.getConnection(DBURL, USERNAME, PASSWORD);
//				PreparedStatement stmt = conn.prepareStatement(UPDATE_STATEMENT);) {
//
//			stmt.setString(1, name);
//			stmt.setString(2, email);
//			stmt.setString(3, address);
//			stmt.setLong(4, customerId);
//
//			stmt.executeUpdate();
//
//		} catch (SQLException e) {
//			//Tratamento de exceção incorreto! Estou escondendo o erro...
//			System.out.println(e.getMessage());
//		}
	}

	@Override
	public boolean delete(long customerId) {

		boolean sucesso = false;

//		try (Connection conn = (Connection) DriverManager.getConnection(DBURL, USERNAME, PASSWORD);
//				PreparedStatement stmt = conn.prepareStatement(DELETE_ONE_STATEMENT);) {
//
//			stmt.setLong(1, customerId);
//
//			sucesso = (stmt.executeUpdate() == 1);
//
//		} catch (SQLException e) {
//			//Tratamento de exceção incorreto! Estou escondendo o erro...
//			System.out.println(e.getMessage());
//		}

		return sucesso;
	}

	@Override
	public Customer getCustomerById(long customerId) {
		Customer customer = null;

//		try (Connection conn = (Connection) DriverManager.getConnection(DBURL, USERNAME, PASSWORD);
//				PreparedStatement stmt = conn.prepareStatement(SELECT_ONE_STATEMENT);) {
//			stmt.setLong(1, customerId);
//			ResultSet rs = stmt.executeQuery();
//
//			if (rs.next()) {
//
//				long resultCustomerId = rs.getLong("CustomerId");
//				String name = rs.getString("Name");
//				String email = rs.getString("Email");
//				String address = rs.getString("Address");
//
//				customer = new Customer(resultCustomerId, name, email, address);
//			}
//
//		} catch (SQLException e) {
//			//Tratamento de exceção incorreto! Estou escondendo o erro...
//			System.out.println(e.getMessage());
//		}

		return customer;
	}

	@Override
	public Set<Ocurrence> getOcurrencies(Long customerId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addOcurrency(long customerId, Ocurrence ocurrency) {
		// TODO Auto-generated method stub
		
	}

}
