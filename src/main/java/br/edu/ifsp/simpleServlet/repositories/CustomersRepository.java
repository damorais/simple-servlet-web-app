package br.edu.ifsp.simpleServlet.repositories;

import br.edu.ifsp.simpleServlet.entities.Customer;
import br.edu.ifsp.simpleServlet.entities.Ocurrence;
import br.edu.ifsp.simpleServlet.entities.Address;

import java.util.List;
import java.util.Set;

public interface CustomersRepository {

	List<Customer> getCustomers();

	void save(String name, String email, Address address);

	void save(long customerId, String name, String email, Address address);

	boolean delete(long customerId);

	Customer getCustomerById(long customerId);

	Set<Ocurrence> getOcurrencies(Long customerId);
	
	void addOcurrency(long customerId, Ocurrence ocurrency);

}