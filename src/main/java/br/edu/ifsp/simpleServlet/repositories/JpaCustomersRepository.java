package br.edu.ifsp.simpleServlet.repositories;

import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.edu.ifsp.simpleServlet.entities.Customer;
import br.edu.ifsp.simpleServlet.entities.Ocurrence;
import br.edu.ifsp.simpleServlet.entities.Address;

public class JpaCustomersRepository implements CustomersRepository {

	//NOTA: Observe a localização do diretorio META-INF, onde está localizado o persistence.xml
	//O papel dele é similar ao do diretório WEB-INF, sendo utilizado para a configuração do 
	//framework.
	//Se estiver no local incorreto, vocês obterão uma exception informando que não é possível localizar 
	//a unidade de persistência (IFSP_CRM)
	@Override
	public List<Customer> getCustomers() {
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("IFSP_CRM");
		
		EntityManager manager = factory.createEntityManager();
		
		//Observe que a consulta usa a linguagem interna da JPA (onde aparece from Customer)
		//Aqui, deve ser seguida a identificação utilizada pela classe, e não pela tabela (note que a tabela chama-se
		//CUSTOMERS
		List<Customer> customers = manager
				.createQuery("from Customer", Customer.class)
				.getResultList(); 
	
		manager.close();
		
		return customers;
	}

	@Override
	public void save(String name, String email, Address address) {
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("IFSP_CRM");
		
		EntityManager manager = factory.createEntityManager();
		
		Customer customer = new Customer(name, email, address);
		
		manager.getTransaction().begin();    
	    manager.persist(customer);
	    manager.getTransaction().commit();  
	    	
		manager.close();
	}

	@Override
	public void save(long customerId, String name, String email, Address address) {
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("IFSP_CRM");
		
		EntityManager manager = factory.createEntityManager();

		//Na operação de update, primeiro recupero a entidade original e depois atualizo os campos.
		//Poderia também criar uma instância nova de Customer, definindo o valor do customerId como o valor
		//que desejo atualizar
		Customer customer = manager.find(Customer.class, customerId);
		
		customer.setName(name);
		customer.setEmail(email);
		customer.setAddress(address);
		
		manager.getTransaction().begin();    
	    manager.persist(customer);
	    manager.getTransaction().commit();  
	    	
		manager.close();
	}

	@Override
	public boolean delete(long customerId) {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("IFSP_CRM");
		
		EntityManager manager = factory.createEntityManager();
		
		Customer customer = manager.find(Customer.class, customerId);
		
		manager.getTransaction().begin();    
	    manager.remove(customer);
	    manager.getTransaction().commit();  
	    	
		manager.close();
		
		return true;
	}

	@Override
	public Customer getCustomerById(long customerId) {
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("IFSP_CRM");
		EntityManager manager = factory.createEntityManager();
		Customer customers = manager.find(Customer.class, customerId);
		manager.close();
		
		return customers;
	}

	@Override
	public Set<Ocurrence> getOcurrencies(Long customerId) {

		
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addOcurrency(long customerId, Ocurrence ocurrency) {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("IFSP_CRM");
		
		EntityManager manager = factory.createEntityManager();

		Customer customer = manager.find(Customer.class, customerId);
		
		manager.getTransaction().begin();   
		manager.persist(ocurrency);
		customer.getOccurrencies().add(ocurrency);
	    manager.persist(customer);
	    manager.getTransaction().commit();  
	    	
		manager.close();
	}
}
