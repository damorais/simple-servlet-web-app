package br.edu.ifsp.simpleServlet.repositories;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import br.edu.ifsp.simpleServlet.entities.Customer;
import br.edu.ifsp.simpleServlet.entities.Ocurrence;
import br.edu.ifsp.simpleServlet.entities.Address;

public class StaticCustomersRepository implements CustomersRepository {

    //Representa a coleção de clientes.
    private static Map<Long, Customer> Customers = new HashMap<>();
    //Utilizo esta propriedade estática para representar o último ID inserido
    private static long lastID = 0l;


    /* (non-Javadoc)
	 * @see br.edu.ifsp.simpleServlet.repositories.CustomerRepository#getCustomers()
	 */
    @Override
	public List<Customer> getCustomers(){

        List<Customer> resultado = new ArrayList<Customer>(StaticCustomersRepository.Customers.values());

        return resultado;

    }

    /* (non-Javadoc)
	 * @see br.edu.ifsp.simpleServlet.repositories.CustomerRepository#save(java.lang.String, java.lang.String, java.lang.String)
	 */
    @Override
	public void save(String name, String email, Address address) {
        //Incremento o ID
    	StaticCustomersRepository.lastID++;

        this.save(StaticCustomersRepository.lastID, name, email, address);
    }

    /* (non-Javadoc)
	 * @see br.edu.ifsp.simpleServlet.repositories.CustomerRepository#save(long, java.lang.String, java.lang.String, java.lang.String)
	 */
    @Override
	public void save(long customerId, String name, String email, Address address){
        //Crio uma nova instância com base no último ID
        Customer customer = new Customer(customerId, name, email, address);
        //Adiciono a coleção
        StaticCustomersRepository.Customers.put(customer.getCustomerId(), customer);
    }


    /* (non-Javadoc)
	 * @see br.edu.ifsp.simpleServlet.repositories.CustomerRepository#delete(long)
	 */
    @Override
	public boolean delete(long customerId){
       Customer removedCustomer = StaticCustomersRepository.Customers.remove(customerId);
       
       return (removedCustomer != null);
    }

    /* (non-Javadoc)
	 * @see br.edu.ifsp.simpleServlet.repositories.CustomerRepository#getCustomerById(long)
	 */
    @Override
	public Customer getCustomerById(long customerId){

        return StaticCustomersRepository.Customers.get(customerId);
    }

	@Override
	public Set<Ocurrence> getOcurrencies(Long customerId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addOcurrency(long customerId, Ocurrence ocurrency) {
		// TODO Auto-generated method stub
		
	}
}
