package br.edu.ifsp.simpleServlet.entities;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="Occurrencies")
public class Ocurrence {
	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
    private long ocurrenceId;
	
	@Column(length=500)
	private String description;
//	
//	@ManyToOne(fetch = FetchType.EAGER)
//	private Customer customer;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(insertable=true, updatable=false)
	private Calendar createdAt;

//	public Customer getCustomer() {
//		return customer;
//	}

	@PrePersist
	protected void onCreate() {
	    createdAt = Calendar.getInstance();
	}
	
//	public void setCustomer(Customer customer) {
//		this.customer = customer;
//	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getOcurrencyId() {
		return ocurrenceId;
	}

	public void setOcurrencyId(long ocurrencyId) {
		this.ocurrenceId = ocurrencyId;
	}
	
	public Ocurrence() {
		
	}
	
	public Ocurrence(String description) {
		this.description = description;
	}

	public Calendar getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Calendar createdAt) {
		this.createdAt = createdAt;
	}
	
//	public String getFormattedCreatedAt() {
//		final DateFormat df = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
//		
//		return df.format(this.getCreatedAt().getTime());
//	}
}
