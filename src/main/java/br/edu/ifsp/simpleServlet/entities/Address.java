package br.edu.ifsp.simpleServlet.entities;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Address {
	
	@Column(length=200,nullable=false)
	private String street;
	@Column(length=100)
	private String complement;	
	@Column(length=100,nullable=false)
	private String city;
	@Column(length=2,nullable=false)
	private String state;

	public String getStreet() {
		return street;
	}
	
	public void setStreet(String street) {
		this.street = street;
	}
	
	public String getCity() {
		return city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public String getState() {
		return state;
	}
	
	public void setState(String state) {
		this.state = state;
	}	

	public String getComplement() {
		return complement;
	}

	public void setComplement(String complement) {
		this.complement = complement;
	}
	
	public Address() {
		
	}
	
	public Address(String street, String complement, String city, String state) {
		this.street = street;
		this.complement = complement;
		this.city = city;
		this.state = state;
	}
	
	@Override
	public String toString() {
		return this.street + " - " + this.city + " - " + this.state;
	}

}
