package br.edu.ifsp.simpleServlet.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Entity
@Table(name="Customers")
public class Customer {

	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
    private long customerId;
	
	@Column(length=100, nullable=false)
	private String name;
	
	@Column(length=50, nullable=false)
	private String email;
	
	@Embedded
	private Address address;
	
	@OneToMany(targetEntity = Ocurrence.class, fetch = FetchType.EAGER)
	private List<Ocurrence> occurrencies;

	
	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public long getCustomerId(){
        return this.customerId;
    }
    
    public String getName(){
        return this.name;
    }
        
    public String getEmail() {
    		return this.email;	
    }
    
    public Address getAddress() {
		return address;
	}   
    
    public Customer() {
    	
    }

    public Customer(long customerId, String name, String email, Address address){
        this.customerId = customerId;
        this.name = name;
        this.email = email;
        this.setAddress(address);
    }
    
    public Customer(String name, String email, Address address) {
    		this.name = name;
    		this.email = email;
    		this.setAddress(address);
    }

	public List<Ocurrence> getOccurrencies() {
		return occurrencies;
	}

	public void setOccurrencies(List<Ocurrence> occurrencies) {
		this.occurrencies = occurrencies;
	}

	
}
