package br.edu.ifsp.simpleServlet.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.edu.ifsp.simpleServlet.entities.Customer;
import br.edu.ifsp.simpleServlet.entities.Ocurrence;
import br.edu.ifsp.simpleServlet.factories.CustomersRepositoryFactory;
import br.edu.ifsp.simpleServlet.repositories.CustomersRepository;

@WebServlet("/clientes/ocorrencias")
public class OcurrenciesServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3686500259299384422L;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws 
		ServletException, IOException {
		
		Long customerId = Long.parseLong(request.getParameter("customerId"));
		
		CustomersRepository repo = CustomersRepositoryFactory.GetCustomerRepository("JPA");
		
		Customer customer = repo.getCustomerById(customerId);
		List<Ocurrence> ocurrencies = customer.getOccurrencies();
		
		request.setAttribute("customer", customer);
		request.setAttribute("ocurrencies", ocurrencies);
		
		String destinationView = "/WEB-INF/views/ocurrencies/list.jsp";
		RequestDispatcher dispatcher = request.getRequestDispatcher(destinationView);
		dispatcher.forward(request, response);
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws 
		ServletException, IOException {
		
		Long customerId = Long.parseLong(request.getParameter("customerId"));
		
		String description = request.getParameter("txtDescription");
		
		if(description != null && !description.equals("")) {
			CustomersRepository repo = CustomersRepositoryFactory.GetCustomerRepository("JPA");
			repo.addOcurrency(customerId, new Ocurrence(description));
			
			request.getSession().setAttribute("flashMessage", "Ocorrência incluída com sucesso!");
		}
		response.sendRedirect("/clientes/ocorrencias?customerId=" + customerId);
	}
	
}
