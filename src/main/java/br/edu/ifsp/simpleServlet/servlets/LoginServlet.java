package br.edu.ifsp.simpleServlet.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {

	private static final long serialVersionUID = 2035359001765841290L;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/views/login/login.jsp");
		rd.forward(request, response);
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String email = request.getParameter("txtEmail");
		String password = request.getParameter("txtPassword");
		
		if(email != null && password != null &&
				email.equals("damorais@gmail.com") && password.equals("123456")) {
			request.getSession().setAttribute("UserAuthenticated", email);
			response.sendRedirect("/clientes");
		}
		else {
			request.getSession().setAttribute("errors", "Usuário ou senha inválidos!");
			response.sendRedirect("/login");
		}
	}
}
