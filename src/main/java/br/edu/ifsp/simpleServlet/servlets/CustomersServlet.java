package br.edu.ifsp.simpleServlet.servlets;

import br.edu.ifsp.simpleServlet.entities.Address;
import br.edu.ifsp.simpleServlet.factories.CustomersRepositoryFactory;
import br.edu.ifsp.simpleServlet.repositories.CustomersRepository;
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/clientes")
public class CustomersServlet extends HttpServlet {

	private static final long serialVersionUID = -4894045511854860673L;

	private CustomersRepository getCustomerRepositoryFromFactory() {
		return CustomersRepositoryFactory.GetCustomerRepository("JPA");
	}
		
	@Override
	public void init() throws ServletException {
		System.out.println("Servlet " + this.getServletName() + " has started");
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String action = request.getParameter("action");
		String destinationPage = "";

		action = (action == null) ? "list" : action.toLowerCase();

		CustomersRepository customersRepo = getCustomerRepositoryFromFactory();;
		RequestDispatcher dispatcher;
		
		switch (action) {
		case "new":
			destinationPage = "/WEB-INF/views/customers/new.jsp";
			dispatcher = request.getRequestDispatcher(destinationPage);
			dispatcher.forward(request, response);
			break;

		case "edit":
			String customerIdToShow = request.getParameter("customerId");
			
			// Adiciona um novo parâmetro, que será consumido pela página JSP.
			request.setAttribute("customer", customersRepo.getCustomerById(Integer.parseInt(customerIdToShow)));

			destinationPage = "/WEB-INF/views/customers/edit.jsp";
			dispatcher = request.getRequestDispatcher(destinationPage);
			dispatcher.forward(request, response);

			break;
		case "delete":
			String customerIdToRemove = request.getParameter("customerId");

			customersRepo.delete(Integer.parseInt(customerIdToRemove));

			// Adiciona um novo parâmetro, que será consumido pela página JSP.
			// Este atributo indica uma mensagem de sucesso
			request.getSession().setAttribute("flashMessage", "Cliente removido com sucesso!");
			response.sendRedirect("/clientes");
			break;
		case "list":
		default:
			// Adiciona um novo parâmetro, que será consumido pela página JSP
			request.setAttribute("customers", customersRepo.getCustomers());
			destinationPage = "/WEB-INF/views/customers/list.jsp";
			dispatcher = request.getRequestDispatcher(destinationPage);
			dispatcher.forward(request, response);
			break;

		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String action = request.getParameter("action");

		action = (action == null) ? "" : action.toLowerCase();

		// TODO: Adicionar validação
		String name = request.getParameter("txtName");
		String email = request.getParameter("txtEmail");
		
		
		String street = request.getParameter("txtStreet");
		String complement = request.getParameter("txtComplement");
		String city = request.getParameter("txtCity");
		String state = request.getParameter("txtState");
		
		Address address = new Address(street, complement, city, state);
				
		CustomersRepository repo = getCustomerRepositoryFromFactory();

		switch (action) {
		case "new":

			repo.save(name, email, address);

			// Adiciona um novo parâmetro, que será consumido pela página JSP.
			// Este atributo indica uma mensagem de sucesso
			request.getSession().setAttribute("flashMessage", "Cliente criado com sucesso!");
			
			break;

		case "edit":
			String customerId = request.getParameter("hdnCustomerId");
						
			repo.save(Integer.parseInt(customerId), name, email, address);

			// Adiciona um novo parâmetro, que será consumido pela página JSP.
			// Este atributo indica uma mensagem de sucesso
			request.getSession().setAttribute("flashMessage", "Cliente alterado com sucesso!");

			break;
		}

		response.sendRedirect("/clientes");
	}

	@Override
	public void destroy() {
		System.out.println("Servlet " + this.getServletName() + " has stopped");
	}

}
